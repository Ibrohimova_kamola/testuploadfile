import {UrlType} from "@/constants/Url";

export const uploadFileType = (file: File): string => {
    if (file.type) {
        if (file.type.includes("png") || file.type.includes("jpeg")) {
            return URL.createObjectURL(file) as string
        } else if (file.type.includes("pdf")) {
            return UrlType.pdf;
        } else if (file.type.includes("zip")) {
            return UrlType.zip;
        } else if (file.type.includes("sql")) {
            return UrlType.sql;
        } else if (file.type.includes("html")) {
            return UrlType.html;
        } else {
            return UrlType.unknown;
        }
    } else {
        return UrlType.unknown;
    }
};