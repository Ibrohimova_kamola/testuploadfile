import {describe, expect, it} from "vitest";
import {mount} from "@vue/test-utils";
import {nextTick} from "vue";
import Multiple from "../multiple/Multiple.vue";

describe('Multiple',()=>{
    it('Check for multiple ', () => {
        const wrapper = mount(Multiple, {props:{
                maxSize: 4,
                isMultiple:true,
                maxFileCount: 3,
            }})
        const input = wrapper.find('input[multiple]')
        expect(input.exists()).toBe(true)
    });
    it('should get maxSize', async () => {
        const wrapper = mount(Multiple, {
            props: {
                maxSize: 4,
                isMultiple:true,
                maxFileCount: 3,
            }
        })
        const inputElement = wrapper.find('input[type="file"]').element as HTMLInputElement
        const file = new File(['54184'], 'foo.txt', {
            type: 'text/plain'
        })
        const mockFileList = Object.create(inputElement.files)
        mockFileList[0] = file
        Object.defineProperty(mockFileList, 'length', { value: 1 })
        ;(wrapper.getCurrentComponent().exposed as unknown as any).filesUpload({
            target: { files: mockFileList }
        })
        await nextTick()
        const errorMessageElement = wrapper.find('p[data-test-error]')
        expect(errorMessageElement.exists()).toBe(false)
    });
    it('Check for max Multiple count', ()=>{
        const wrapper = mount(Multiple, {props:{
                maxSize: 4,
                isMultiple:true,
                maxFileCount: 3,
            }})
        const text = wrapper.find('p[data-test-warning-message]')
        expect(text.exists()).toBe(false)
    })
})