import {describe, expect, it} from "vitest";
import {uploadFileType} from "../../type/FileType";
import {UrlType} from "../../constants/Url";


describe('File type',()=>{

    it('For PDF file type', () => {
        const file = {type: 'pdf'} as File;
        const res = uploadFileType(file);
        expect(res).toBe(UrlType.pdf);
    });

    it('For ZIP file type', () => {
        const file = {type: 'zip'} as File;
        const res = uploadFileType(file);
        expect(res).toBe(UrlType.zip)
    })

    it('For SQL file type', () => {
        const file = {type: 'sql'} as File;
        const res = uploadFileType(file)
        expect(res).toBe(UrlType.sql)
    })

    it('For HTML file type', () => {
        const file = {type: 'html'} as File;
        const res = uploadFileType(file)
        expect(res).toBe(UrlType.html)
    })
    it('For unknown file type', () => {
        const file = {type: ''} as File;
        const res = uploadFileType(file)
        expect(res).toBe(UrlType.unknown)
    })
    it('If file don`t come', () => {
        const file = {} as File;
        const res = uploadFileType(file)
        expect(res).toBe(UrlType.unknown)
    })

})