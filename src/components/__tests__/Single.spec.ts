import {describe, expect, it} from "vitest";
import {mount} from "@vue/test-utils";
import Single from "../single/Single.vue";
import {nextTick} from "vue";

describe('Single', () => {
    it('should render input', () => {
        const wrapper = mount(Single)
        expect(wrapper.find('input').html()).matchSnapshot()
    })
    it('should get maxSize', async () => {
        const wrapper = mount(Single, {
            props: {
                maxSize: 4
            }
        })
        const inputElement = wrapper.find('input[type="file"]').element as HTMLInputElement
        const file = new File(['54184'], 'foo.txt', {
            type: 'text/plain'
        })
        const mockFileList = Object.create(inputElement.files)
        mockFileList[0] = file
        Object.defineProperty(mockFileList, 'length', {value: 1});
        await (wrapper.getCurrentComponent().exposed as unknown as any).handleFileUpload({
            target: {files: mockFileList}
        })
        await nextTick()
        const errorMessageElement = wrapper.find('[data-test-error]')
        expect(errorMessageElement.exists()).toBe(true)
    });
})
