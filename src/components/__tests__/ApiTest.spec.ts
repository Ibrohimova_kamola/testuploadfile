import {describe, expect, it} from "vitest";
import {uploadFile} from "../../service/File.service";

describe('UploadFile',()=>{
    it('upload file successfully', async () => {
        const file = new File(['file content'], 'test-file.txt', {type: 'text/plain'});
        const response = await uploadFile(file);
        expect(response.status).toBe(200)
    });
})